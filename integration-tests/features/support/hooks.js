/* hooks.js
 *
 * https://github.com/cucumber/cucumber-js/blob/master/docs/support_files/hooks.md
 */

var { After } = require('@cucumber/cucumber')

After(function () {
  return this.driver.quit()
});
